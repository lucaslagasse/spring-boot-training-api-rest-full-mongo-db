package br.com.mdsistemas.training.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import br.com.mdsistemas.training.databuilders.domains.DomainsTemplateLoader;
import br.com.mdsistemas.training.databuilders.domains.TaskTemplate;
import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.TaskDatabaseGateway;
import br.com.mdsistemas.training.usecases.UpdateTaskUseCase;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

public class UpdateTaskUseCaseUnitTest {
	
	@InjectMocks
	private UpdateTaskUseCase updateTaskUseCase;
	
	@Mock
	private TaskDatabaseGateway taskDatabaseGateway;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeClass
	public static void init() {
		FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainsTemplateLoader.class));
	}
	
	@Test
	public void updateWithSuccess() {
		
		final Task taskToUpdate = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_CREATED);
		
		//final String id = taskToUpdate.getId();
		Boolean isDone = taskToUpdate.getIsDone();
		
		//final Boolean NewIsDone = !taskToUpdate.getIsDone();
		
		Task taskUpdated = taskToUpdate;
		
		taskUpdated.setIsDone(!isDone);
		
		when(this.taskDatabaseGateway.update(taskToUpdate.getId(), !isDone)).thenReturn(taskUpdated);
		
		final Task taskResponse = this.updateTaskUseCase.update(taskToUpdate.getId(), !isDone);
		
		final ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
		
		verify(this.taskDatabaseGateway, VerificationModeFactory.times(0)).update(taskUpdated.getId(), !isDone);
		
		final Task taskCaptured = taskCaptor.getValue();
		
		assertEquals(taskToUpdate.getDate(), taskCaptured.getDate());
		assertEquals(taskToUpdate.getDescription(), taskCaptured.getDescription());
		assertEquals(taskToUpdate.getId(), taskCaptured.getId());
		
		assertNotEquals(taskToUpdate.getIsDone(), taskCaptured.getIsDone());
		
		assertEquals(taskUpdated.getDate(), taskResponse.getDate());
		assertEquals(taskUpdated.getDescription(), taskResponse.getDescription());
		assertEquals(taskUpdated.getId(), taskResponse.getId());
		assertEquals(taskUpdated.getIsDone(), taskResponse.getIsDone());
		
		
		
		
		
	}
	
	

}
