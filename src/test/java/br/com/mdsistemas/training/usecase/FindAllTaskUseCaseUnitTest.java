package br.com.mdsistemas.training.usecase;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.mdsistemas.training.databuilders.domains.DomainsTemplateLoader;
import br.com.mdsistemas.training.databuilders.domains.TaskTemplate;
import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.TaskDatabaseGateway;
import br.com.mdsistemas.training.usecases.CreateTaskUseCase;
import br.com.mdsistemas.training.usecases.DeleteTaskUseCase;
import br.com.mdsistemas.training.usecases.FindAllTaskUseCase;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

public class FindAllTaskUseCaseUnitTest {
	
	@InjectMocks
	private FindAllTaskUseCase findAllTaskUseCase;
	
	@Mock
	private TaskDatabaseGateway taskDatabaseGateway;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeClass
	public static void init() {
		FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainsTemplateLoader.class));
	}
	
	
	@Test
	public void findAllWithSuccess() {
		
		final List<Task> taskList = Fixture.from(Task.class).gimme(2, TaskTemplate.TEMPLATE_LIST);
		
		doReturn(taskList).when(this.taskDatabaseGateway).findAll();
		
		final List<Task> tasksReturned = this.findAllTaskUseCase.findAll();
		
		findAllAssertsEquals(taskList, tasksReturned);	
		
	} 
	
	private void findAllAssertsEquals(final List<Task> taskList, final List<Task> tasksReturned) {
		
		assertEquals(taskList.size(), tasksReturned.size());
		assertEquals(taskList.get(0).getId(), tasksReturned.get(0).getId());
		assertEquals(taskList.get(0).getDescription(), tasksReturned.get(0).getDescription());
		assertEquals(taskList.get(0).getDate(), tasksReturned.get(0).getDate());
		assertEquals(taskList.get(0).getIsDone(), tasksReturned.get(0).getIsDone());
		
		assertEquals(taskList.get(1).getId(), tasksReturned.get(1).getId());
		assertEquals(taskList.get(1).getDescription(), tasksReturned.get(1).getDescription());
		assertEquals(taskList.get(1).getDate(), tasksReturned.get(1).getDate());
		assertEquals(taskList.get(1).getIsDone(), tasksReturned.get(1).getIsDone());
			
	}

}
