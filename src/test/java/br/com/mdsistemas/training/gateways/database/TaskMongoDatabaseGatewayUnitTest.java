package br.com.mdsistemas.training.gateways.database;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.stubbing.OngoingStubbing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.any;

import br.com.mdsistemas.training.databuilders.domains.DomainsTemplateLoader;
import br.com.mdsistemas.training.databuilders.domains.TaskTemplate;
import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.mongo.TaskMongoDatabaseGateway;
import br.com.mdsistemas.training.gateways.database.task.mongo.repository.TaskRepository;
import br.com.mdsistemas.training.gateways.exceptions.ErrorToAccessDatabaseGatewayException;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

public class TaskMongoDatabaseGatewayUnitTest {
	
	@InjectMocks
	private TaskMongoDatabaseGateway taskMongoDatabaseGateway;
	
	@Mock
	private TaskRepository taskRepository;
	
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeClass
	public static void init() {
		FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainsTemplateLoader.class));
	}
	
	@Test
	public void createWithSuccess() {
		
		final Task taskToCreated = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_TO_CREATE);
		final Task taskCreated = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_CREATED);
		
		when(this.taskRepository.save(any(Task.class))).thenReturn(taskCreated);
		
		final Task taskResponse = this.taskMongoDatabaseGateway.create(taskToCreated);
		
		final ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
		
		verify(this.taskRepository, VerificationModeFactory.times(1)).save(taskCaptor.capture());
		
		final Task taskCaptured = taskCaptor.getValue();
		
		assertEquals(taskToCreated.getDate(), taskCaptured.getDate());
		assertEquals(taskToCreated.getDescription(), taskCaptured.getDescription());
		assertEquals(null, taskCaptured.getId());
		assertEquals(false, taskCaptured.getIsDone());
		
		assertEquals(taskCreated.getDate(), taskResponse.getDate());
		assertEquals(taskCreated.getDescription(), taskResponse.getDescription());
		assertEquals(taskCreated.getId(), taskResponse.getId());
		assertEquals(taskCreated.getIsDone(), taskResponse.getIsDone());		
	}
	
	@Test(expected = ErrorToAccessDatabaseGatewayException.class)
	public void createWithErrorToAccessDatabase() {
		
		final Task taskToCreated = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_TO_CREATE);
				
		doThrow(new RuntimeException()).when(this.taskRepository).save(any(Task.class));
		
		try {
			this.taskMongoDatabaseGateway.create(taskToCreated);
		} catch (ErrorToAccessDatabaseGatewayException e) {
			assertEquals("training.errorToAccessDatabase", e.id().code());
			assertEquals("Error to access database.", e.id().message());			
			
			throw e;
		}			
	}
	
	@Test
	public void deleteWithSuccess() {
		
		final String taskId = "123L";
		
        this.taskMongoDatabaseGateway.delete(taskId);
        
        verify(this.taskRepository, VerificationModeFactory.times(1)).deleteById(taskId);
		
		
	}
	
	@Test(expected = ErrorToAccessDatabaseGatewayException.class)
	public void deleteWithErrorToAccessDatabase() {
		
		final String taskId = "123L";
		
		try {
			doThrow(new RuntimeException()).when(this.taskRepository).deleteById(taskId);
			this.taskMongoDatabaseGateway.delete(taskId);	
			
		} catch (ErrorToAccessDatabaseGatewayException e) {
			assertEquals("training.errorToAccessDatabase", e.id().code());
			assertEquals("Error to access database.", e.id().message());			
			
			throw e;
		}	
		
	}
	
	@Test
	public void findAllWithSuccess() {
		
		final List<Task> taskList = Fixture.from(Task.class).gimme(2, TaskTemplate.TEMPLATE_LIST);
		
		doReturn(taskList).when(this.taskRepository).findAll();
		
		final List<Task> tasksReturned = this.taskMongoDatabaseGateway.findAll();
		
		findAllAssertsEquals(taskList, tasksReturned);	
		
	}
	
	@Test(expected = ErrorToAccessDatabaseGatewayException.class)
	public void findAllWithErrorToAccessDatabase() {
	
		try {	
			doThrow(new RuntimeException()).when(this.taskRepository).findAll();
			this.taskMongoDatabaseGateway.findAll();
			
		} catch (ErrorToAccessDatabaseGatewayException e) {
			assertEquals("training.errorToAccessDatabase", e.id().code());
			assertEquals("Error to access database.", e.id().message());			
			
			throw e;
		}	
		
	}
	
	@Test
	public void updateWithSuccess() {
		
		final Task taskToUpdate = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_CREATED);
		
		final String id = taskToUpdate.getId();
		final Boolean isDone = taskToUpdate.getIsDone();
		
		this.taskMongoDatabaseGateway.update(id, isDone);
		
		ArgumentCaptor<Task> taskAC = ArgumentCaptor.forClass(Task.class);
		verify(this.taskRepository).save(taskAC.capture());
		
		final Task taskToUpdated = taskAC.getValue(); 
				
		assertEquals(taskToUpdate.getId(), taskToUpdated.getId());
		assertEquals(taskToUpdate.getDescription(), taskToUpdated.getDescription());
		assertEquals(taskToUpdate.getDate(), taskToUpdated.getDate());
		assertEquals(taskToUpdate.getIsDone(), taskToUpdated.getIsDone());
		
		
		
	}
	
	@Test(expected = ErrorToAccessDatabaseGatewayException.class)
	public void updateWithError() {
		
		
	}

	private void findAllAssertsEquals(final List<Task> taskList, final List<Task> tasksReturned) {
		
		assertEquals(taskList.size(), tasksReturned.size());
		assertEquals(taskList.get(0).getId(), tasksReturned.get(0).getId());
		assertEquals(taskList.get(0).getDescription(), tasksReturned.get(0).getDescription());
		assertEquals(taskList.get(0).getDate(), tasksReturned.get(0).getDate());
		assertEquals(taskList.get(0).getIsDone(), tasksReturned.get(0).getIsDone());
		
		assertEquals(taskList.get(1).getId(), tasksReturned.get(1).getId());
		assertEquals(taskList.get(1).getDescription(), tasksReturned.get(1).getDescription());
		assertEquals(taskList.get(1).getDate(), tasksReturned.get(1).getDate());
		assertEquals(taskList.get(1).getIsDone(), tasksReturned.get(1).getIsDone());
		
	}
	
	

}
