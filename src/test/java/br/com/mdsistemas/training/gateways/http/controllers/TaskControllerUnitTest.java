package br.com.mdsistemas.training.gateways.http.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.any;

import br.com.mdsistemas.training.databuilders.domains.DomainsTemplateLoader;
import br.com.mdsistemas.training.databuilders.domains.TaskTemplate;
import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.http.controllers.json.FindAllTaskJsonResponse;
import br.com.mdsistemas.training.gateways.http.controllers.json.TaskJsonRequest;
import br.com.mdsistemas.training.gateways.http.controllers.json.TaskJsonResponse;
import br.com.mdsistemas.training.usecases.CreateTaskUseCase;
import br.com.mdsistemas.training.usecases.DeleteTaskUseCase;
import br.com.mdsistemas.training.usecases.FindAllTaskUseCase;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

public class TaskControllerUnitTest {
	
	@InjectMocks
	private TaskController taskController;
	
	@Mock
	private CreateTaskUseCase createTaskUseCase;
	
	@Mock
	private DeleteTaskUseCase deleteTaskUseCase;
	
	@Mock
	private FindAllTaskUseCase findAllTaskUseCase;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeClass
	public static void init() {
		FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainsTemplateLoader.class));
	}

	
	@Test
	public void createWithSuccess() {
		
		final TaskJsonRequest taskJsonRequest = new TaskJsonRequest("anyDescripton", LocalDate.parse("2020-02-02"));
		final Task taskCreated = Fixture.from(Task.class).gimme(TaskTemplate.TEMPLATE_CREATED);
		
		when(this.createTaskUseCase.create(any(Task.class))).thenReturn(taskCreated);
		
		final TaskJsonResponse taskResponse = this.taskController.create(taskJsonRequest);
		
		final ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
		
		verify(this.createTaskUseCase, VerificationModeFactory.times(1)).create(taskCaptor.capture());
		
		final Task taskCaptured = taskCaptor.getValue();
		
		assertEquals(taskJsonRequest.getDate(), taskCaptured.getDate());
		assertEquals(taskJsonRequest.getDescription(), taskCaptured.getDescription());
		assertEquals(null, taskCaptured.getId());
		assertEquals(false, taskCaptured.getIsDone());
		
		assertEquals(taskCreated.getDate(), taskResponse.getDate());
		assertEquals(taskCreated.getDescription(), taskResponse.getDescription());
		assertEquals(taskCreated.getId(), taskResponse.getId());
		assertEquals(taskCreated.getIsDone(), taskResponse.getIsDone());
		
	}
	
	@Test
	public void findAllWithSuccess() {
		
		final List<Task> taskList = Fixture.from(Task.class).gimme(2, TaskTemplate.TEMPLATE_LIST);
		
		doReturn(taskList).when(this.findAllTaskUseCase).findAll();
		
		final FindAllTaskJsonResponse tasksReturn = this.taskController.findAll();
		
	 	final List<Task> tasksReturned = tasksReturn.getTasks();
		
		findAllAssertsEquals(taskList, tasksReturned);	
		
	}
	
	@Test
	public void deleteWithSuccess(){
		
		final String taskId = "123L";
		
		final TaskJsonResponse taskResponse = this.taskController.delete(taskId);
		       
        verify(this.deleteTaskUseCase, VerificationModeFactory.times(1)).delete(taskId);
        
        assertEquals(taskResponse, null);
		
	}
	
	private void findAllAssertsEquals(final List<Task> taskList, final List<Task> tasksReturned) {
		
		assertEquals(taskList.size(), tasksReturned.size());
		assertEquals(taskList.get(0).getId(), tasksReturned.get(0).getId());
		assertEquals(taskList.get(0).getDescription(), tasksReturned.get(0).getDescription());
		assertEquals(taskList.get(0).getDate(), tasksReturned.get(0).getDate());
		assertEquals(taskList.get(0).getIsDone(), tasksReturned.get(0).getIsDone());
		
		assertEquals(taskList.get(1).getId(), tasksReturned.get(1).getId());
		assertEquals(taskList.get(1).getDescription(), tasksReturned.get(1).getDescription());
		assertEquals(taskList.get(1).getDate(), tasksReturned.get(1).getDate());
		assertEquals(taskList.get(1).getIsDone(), tasksReturned.get(1).getIsDone());
		
	}
	
}
