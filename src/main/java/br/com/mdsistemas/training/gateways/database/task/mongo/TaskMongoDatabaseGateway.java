package br.com.mdsistemas.training.gateways.database.task.mongo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;


import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.TaskDatabaseGateway;
import br.com.mdsistemas.training.gateways.database.task.mongo.repository.TaskRepository;
import br.com.mdsistemas.training.gateways.exceptions.TaskNotFoundException;
import br.com.mdsistemas.training.gateways.exceptions.ErrorToAccessDatabaseGatewayException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TaskMongoDatabaseGateway implements TaskDatabaseGateway {

	@Autowired
	private TaskRepository taskRepository;

	public Task create(final Task task) {
		
		try {
			log.trace("taks: {}", task);
			
			final Task taskCreated = this.taskRepository.save(task);
			
			return taskCreated;
			
		} catch (Exception error) {
			log.error("Error: {}", error);
			throw new ErrorToAccessDatabaseGatewayException();			
		}		
	}
	
	public List<Task> findAll(){
		
		try {	
			final List<Task> allTasksListed = this.taskRepository.findAll();
			
			log.trace("tasks:{}", allTasksListed);
			
			return allTasksListed;
					
		} catch (Exception error) {
			log.error("Error: {}", error);
			throw new ErrorToAccessDatabaseGatewayException();
			
		}
	}
	
	public void delete(final String id) {
			
		try {
			log.trace("taks: {}", id);
			this.taskRepository.deleteById(id);
				
		} catch (Exception error) {
			log.error("Error: {}", error);
			throw new ErrorToAccessDatabaseGatewayException();			
		}
		
	}
	
	@Override
	public Task update(String id, Boolean isDone) {
		
		try {
			log.trace("start {}", id);		
			
			final Optional<Task> taskListed = this.taskRepository.findById(id);
			
			if(taskListed.isPresent()) {
				final LocalDate date = taskListed.get().getDate();	
				final String description = taskListed.get().getDescription();
				
				final Task taskToUpdate = new Task(id,description,date,isDone);
				final Task taskUpdated = this.taskRepository.save(taskToUpdate);
				
				log.trace("end",taskUpdated);
				
				return taskUpdated;
				
			} else {
				throw new TaskNotFoundException();	
			}
			
		} catch (TaskNotFoundException e) {
			log.error("Error: ", "ID não encontrado!");
			throw new TaskNotFoundException();
			
		} catch (Exception e) {
			log.error("Error: ", e);
			throw new ErrorToAccessDatabaseGatewayException();
		} 
	}
	
}
