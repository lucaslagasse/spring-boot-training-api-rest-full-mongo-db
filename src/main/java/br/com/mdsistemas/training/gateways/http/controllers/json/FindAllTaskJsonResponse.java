package br.com.mdsistemas.training.gateways.http.controllers.json;

import java.time.LocalDate;
import java.util.List;

import br.com.mdsistemas.training.domains.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class FindAllTaskJsonResponse {

	private List<Task> tasks;
	//private List<TaskJsonResponse> tasks;
	
}
