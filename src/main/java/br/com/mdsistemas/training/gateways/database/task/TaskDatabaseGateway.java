package br.com.mdsistemas.training.gateways.database.task;

import java.util.List;
import java.util.Optional;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.exceptions.TaskNotFoundException;

public interface TaskDatabaseGateway {
	
	Task create(final Task task);
	
	void delete(final String id);

	List<Task> findAll();
	
	Task update(final String id, final Boolean isDone);
	
	//Optional<Task> findById(final String id);

}
