package br.com.mdsistemas.training.gateways.exceptions;

import org.springframework.http.HttpStatus;

import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.ExceptionId;
import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.GatewayException;

public class ErrorNotImplementedException extends GatewayException {

	private static final long serialVersionUID = 5331230750358793636L;

	public ErrorNotImplementedException () {
		
		super(ExceptionId.create("training.task.NotImplemented", "Error Not Implemented."));
		
	}
	
	@Override
	public HttpStatus httpStatus() {
		return HttpStatus.NOT_IMPLEMENTED;
	}

	@Override
	public int httpStatusCode() {
		return HttpStatus.NOT_IMPLEMENTED.value();
	}

}