package br.com.mdsistemas.training.gateways.exceptions;

import org.springframework.http.HttpStatus;

import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.ExceptionId;
import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.GatewayException;

public class TaskNotFoundException extends GatewayException {

	private static final long serialVersionUID = 5331230750358793636L;

	public TaskNotFoundException () {
		
		super(ExceptionId.create("training.taskNotFound", "Error task Not Found."));
		
	}
	
	@Override
	public HttpStatus httpStatus() {
		return HttpStatus.NOT_FOUND;
	}

	@Override
	public int httpStatusCode() {
		return HttpStatus.NOT_FOUND.value();
	}

}