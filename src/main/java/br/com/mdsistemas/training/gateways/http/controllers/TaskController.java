package br.com.mdsistemas.training.gateways.http.controllers;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.exceptions.ErrorNotImplementedException;
import br.com.mdsistemas.training.gateways.http.controllers.json.FindAllTaskJsonResponse;
import br.com.mdsistemas.training.gateways.http.controllers.json.TaskJsonRequest;
import br.com.mdsistemas.training.gateways.http.controllers.json.TaskJsonResponse;
import br.com.mdsistemas.training.gateways.http.controllers.json.UpdateIsDoneJsonRequest;
import br.com.mdsistemas.training.usecases.CreateTaskUseCase;
import br.com.mdsistemas.training.usecases.DeleteTaskUseCase;
import br.com.mdsistemas.training.usecases.FindAllTaskUseCase;
import br.com.mdsistemas.training.usecases.UpdateTaskUseCase;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("${baseurl.v1}/tasks")
public class TaskController {

	@Autowired
	private CreateTaskUseCase createTaskUseCase;
	
	@Autowired
	private FindAllTaskUseCase findAllTaskUseCase;
	
	@Autowired
	private DeleteTaskUseCase deleteTaskUseCase;
	
	@Autowired
	private UpdateTaskUseCase updateTaskUseCase;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping	
	public TaskJsonResponse create(
			final @RequestBody(required = true) @NotNull @Valid TaskJsonRequest taskJsonRequest) {

		log.trace("taskJsonRequest: {}", taskJsonRequest);

		final Task taskToBeCreate = new Task(taskJsonRequest.getDescription(), taskJsonRequest.getDate());
		
		final Task taskCreated = this.createTaskUseCase.create(taskToBeCreate);

		final TaskJsonResponse response = new TaskJsonResponse(taskCreated.getId(), taskCreated.getDescription(),
				taskCreated.getDate(), taskCreated.getIsDone());
		
		return response;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public FindAllTaskJsonResponse findAll() {
		
		final List<Task> allTasksListed = this.findAllTaskUseCase.findAll();
		
		final FindAllTaskJsonResponse response = new FindAllTaskJsonResponse(allTasksListed);
		
		return response;
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(value = "/{id}")
	public TaskJsonResponse delete(@PathVariable (required = true) final String id) {
		
		this.deleteTaskUseCase.delete(id);
		
		return null;				
	}
	
	@ResponseStatus(HttpStatus.OK) 
	@PatchMapping(value = "/{id}")
	public TaskJsonResponse update(
			final @RequestBody(required = true) @NotNull @Valid UpdateIsDoneJsonRequest updateIsDoneJsonRequest, 
			@PathVariable (required = true) final String id) {
		
		log.trace("idUpdateJsonRequest: {}", id);
		log.trace("updateJsonRequest: {}", updateIsDoneJsonRequest);
		
		final Boolean isDone = updateIsDoneJsonRequest.getIsDone();
		
		//final Task taskToUpdate = new Task(id,null,null,isDone);
		
		final Task taskUpdated = this.updateTaskUseCase.update(id, isDone);
				
		final TaskJsonResponse response = new TaskJsonResponse(taskUpdated.getId(), taskUpdated.getDescription(),
				taskUpdated.getDate(), taskUpdated.getIsDone());
		
		return response;				
	}
	
	@ResponseStatus(HttpStatus.NOT_IMPLEMENTED) 
	@PutMapping(value = "/{id}")
	public TaskJsonResponse notImplementedPutMapping(@PathVariable (required = true) final String id) {

		log.error("Error: ", "Método não implementado");
			
		return null;
	}
	
	@ResponseStatus(HttpStatus.NOT_IMPLEMENTED) 
	@GetMapping(value = "/{id}")
	public TaskJsonResponse notImplementedGetMappingId(@PathVariable (required = true) final String id) {

		log.error("Error: ", "Método não implementado");
				
		return null;
	}

}
