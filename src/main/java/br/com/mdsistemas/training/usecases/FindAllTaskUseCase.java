package br.com.mdsistemas.training.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.TaskDatabaseGateway;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FindAllTaskUseCase {
	
	@Autowired
	private TaskDatabaseGateway taskDatabaseGateway;
	
	public List<Task> findAll() {
		
		log.trace("task: {}");
		
		return this.taskDatabaseGateway.findAll();
	}
	
}
