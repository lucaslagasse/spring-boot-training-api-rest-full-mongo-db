package br.com.mdsistemas.training.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.gateways.database.task.TaskDatabaseGateway;
import br.com.mdsistemas.training.gateways.exceptions.TaskNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DeleteTaskUseCase {
	
	@Autowired
	private TaskDatabaseGateway taskDatabaseGateway;
	
	public void delete(final String id) {
		
		log.trace("task: {}", id);
		
		this.taskDatabaseGateway.delete(id);		
	}

}
